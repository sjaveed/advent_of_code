#!/usr/bin/env ruby

codes = File.readlines('../../data/2021/input3.txt')
MAX_BITS = 12

codes_to_process = codes.map! { |code| code.to_i(2) }

def calculate_rating(codes_to_process, &block)
  bit = MAX_BITS - 1

  while codes_to_process.size > 1 && bit >= 0
    bit_frequencies = [0, 0]
    bit_mask = 1 << bit

    codes_to_process.each do |code|
      bit_value = code & bit_mask == 0 ? 0 : 1
      bit_frequencies[bit_value] += 1
    end

    chosen_bit = yield(bit_frequencies) << bit
    codes_to_process.select! { |code| code & bit_mask == chosen_bit }
    bit -= 1
  end

  codes_to_process[0]
end

oxygen_generator_rating = calculate_rating(codes_to_process.dup) do |bit_frequencies|
  if bit_frequencies[0] <= bit_frequencies[1]
    1
  else
    0
  end
end

co2_scrubber_rating = calculate_rating(codes_to_process.dup) do |bit_frequencies|
  if bit_frequencies[0] <= bit_frequencies[1]
    0
  else
    1
  end
end

puts "Oxygen Generator Rating: #{oxygen_generator_rating}"
puts "CO2 Scrubber Rating: #{co2_scrubber_rating}"
puts "Product: #{oxygen_generator_rating * co2_scrubber_rating}"
