#!/usr/bin/env ruby

Path = Struct.new 'Path', :to, :traversal_count
Cave = Struct.new 'Cave', :name, :is_large, :paths_by_destination

# Map of a cave system that records traversals
class CaveSystemMap
  attr_accessor :entrance

  def initialize(lines)
    caves_by_name = {}

    lines.each do |line|
      source_cave_name, destination_cave_name = line.split('-')

      source_cave = caves_by_name[source_cave_name] || begin
        caves_by_name[source_cave_name] = Cave.new(source_cave_name, big_cave?(source_cave_name), {})
      end

      destination_cave = caves_by_name[destination_cave_name] || begin
        caves_by_name[destination_cave_name] = Cave.new(destination_cave_name, big_cave?(destination_cave_name), {})
      end

      source_cave.paths_by_destination[destination_cave_name] = Path.new(destination_cave, 0)
      destination_cave.paths_by_destination[source_cave_name] = Path.new(source_cave, 0)
    end

    @entrance = caves_by_name['start']
  end

  def enumerate_paths
    found_paths = []
    journeys_to_end(@entrance, found_paths: found_paths)

    found_paths
  end

  private

  def big_cave?(cave_name)
    cave_name.upcase == cave_name
  end

  def journeys_to_end(starting_cave, existing_journey = [], found_paths: [])
    if starting_cave.name == 'end'
      found_paths << existing_journey + ['end']
      return
    end
    return if starting_cave.name == 'start' && existing_journey.any?

    available_paths = starting_cave.paths_by_destination.values.select do |path|
      path.to.is_large || existing_journey.none? { |traversed_cave_name| traversed_cave_name == path.to.name }
    end

    return if available_paths.empty?

    available_paths.map do |path|
      journeys_to_end(path.to, existing_journey + [starting_cave.name], found_paths: found_paths)
    end
  end
end

input = File.readlines('../../data/2021/input12.txt')
# input = File.readlines('../../spec/fixtures/2021/input12.txt')
cave_system_map = CaveSystemMap.new(input.map(&:chomp))

journeys = cave_system_map.enumerate_paths

puts "Answer: #{journeys.size} journeys"
puts journeys.map { |journey| journey.join(',') }.join("\n")
