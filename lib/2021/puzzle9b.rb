#!/usr/bin/env ruby
require 'set'
require 'rainbow'

BASIN_COLORS = %w[red yellow blue green magenta cyan white].freeze

class HeightMap
  attr_accessor :basins, :low_points

  def initialize(heights)
    @heights = heights
    @low_points = []
    @basin_affinity = heights.size.times.map { [nil] * heights[0].size }
    @basins = []
  end

  def calculate_low_points
    @heights.each_with_index do |row, row_idx|
      row.each_with_index do |col, col_idx|
        if col < neighbors(col_idx, row_idx).min
          @low_points << [row_idx, col_idx]
        end
      end
    end
  end

  def calculate_basins
    basin_idx = 0
    @low_points.each do |row, col|
      basin = Set.new()
      basin << [row, col]
      @basin_affinity[row][col] = basin_idx

      members = mark_basin_members(basin_idx, row, col, neighbor_locations(col, row))
      members.each { |member| basin << member }

      @basins << basin
      basin_idx += 1
    end
  end

  def total_risk_level
    low_point_values.map { |low_point_value| low_point_value + 1 }.reduce(:+)
  end

  def display
    @heights.each_with_index.map do |row, row_idx|
      row.each_with_index.map do |col, col_idx|
        basin_idx = @basin_affinity[row_idx][col_idx]

        if basin_idx.nil?
          col
        else
          basin_color = BASIN_COLORS[basin_idx % BASIN_COLORS.size]
          Rainbow(col).send(basin_color.to_sym)
        end
      end.join('')
    end.join("\n")
  end

  private

  def mark_basin_members(basin_idx, row, col, neighbors_to_consider)
    reference_height = @heights[row][col]

    selected_neighbors = neighbors_to_consider.select do |neighbor_row, neighbor_col|
      @basin_affinity[neighbor_row][neighbor_col].nil? &&
        @heights[neighbor_row][neighbor_col] < 9 &&
        @heights[neighbor_row][neighbor_col] >= reference_height
    end

    updated_selected_neighbors = selected_neighbors.map do |neighbor_row, neighbor_col|
      @basin_affinity[neighbor_row][neighbor_col] = basin_idx

      mark_basin_members(basin_idx, neighbor_row, neighbor_col, neighbor_locations(neighbor_col, neighbor_row))
    end

    (selected_neighbors + updated_selected_neighbors.reduce(:concat).to_a).reject(&:empty?)
  end

  def low_point_values
    @low_points.map { |row, col| @heights[row][col] }
  end

  def neighbor_locations(col, row)
    locations = []

    case col
    when 0
      locations << [row, col + 1]
    when max_col
      locations << [row, col - 1]
    else
      locations << [row, col - 1]
      locations << [row, col + 1]
    end

    case row
    when 0
      locations << [row + 1, col]
    when max_row
      locations << [row - 1, col]
    else
      locations << [row - 1, col]
      locations << [row + 1, col]
    end

    locations
  end

  def neighbors(col, row)
    neighbor_locations(col, row).map { |row, col| @heights[row][col] }
  end

  def max_row
    @max_row ||= @heights.size - 1
  end

  def max_col
    @max_col ||= @heights[0].size - 1
  end
end

input = File.readlines('../../data/2021/input9.txt')
# input = File.readlines('../../spec/fixtures/2021/input9.txt')
heights = input.map { |line| line.chomp.split('').map(&:to_i) }
height_map = HeightMap.new(heights)
height_map.calculate_low_points
height_map.calculate_basins

puts height_map.display

puts "Answer: #{height_map.basins.map(&:size).max(3).reduce(:*)}"
