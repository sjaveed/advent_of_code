#!/usr/bin/env ruby

class Board
  attr_accessor :rows

  def initialize(rows = [])
    @rows = rows
    @cols = rows.transpose

    @by_number = {}
    @rows.flatten.each { |num| @by_number[num[:value]] = num }
  end

  def mark(number)
    return unless by_number.key? number

    by_number[number][:selected] = true
  end

  def winner?
    # Check each row
    return true if rows.any? do |row|
      row.all? { |item| item[:selected] }
    end

    # Check each column
    cols.any? do |col|
      col.all? { |item| item[:selected] }
    end
  end

  def score(number)
    unselected_numbers = by_number
      .values
      .reject { |item| item[:selected] }
      .map { |item| item[:value] }

    return 0 unless unselected_numbers.any?

    number * unselected_numbers.reduce(:+)
  end

  private

  attr_accessor :cols, :by_number
end

def extract_called_numbers(input)
  input[0].split(',').map(&:to_i)
end

def extract_boards(input)
  boards = []
  last_board = []

  input[2..].each do |line|
    if line.chomp == ''
      # We encountered a gap between boards => let's save the previous board
      boards << Board.new(last_board)
      last_board = []
      next
    end

    # We're in the middle of a board => let's parse it
    last_board << line.gsub(/^ */, '').split(/ +/).map do |num|
      { selected: false, value: num.to_i }
    end
  end

  boards << Board.new(last_board) if last_board.size > 0

  boards
end

input = File.readlines('../../data/2021/input4.txt')
called_numbers = extract_called_numbers(input)
boards = extract_boards(input)

puts "Boards Found: #{boards.size}"

winning_boards = {}
last_winner = nil

called_numbers.each do |num|
  boards.each_with_index do |board, board_idx|
    board.mark(num)

    if board.winner?
      unless winning_boards.key? board_idx
        # We've not seen this before
        winning_boards[board_idx] = true
        last_winner = { board_idx: board_idx, score: board.score(num) }
      end
    end
  end
end

puts "Last Winner: ##{last_winner[:board_idx]}, Score: #{last_winner[:score]}"
