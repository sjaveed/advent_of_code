#!/usr/bin/env ruby

class VentMap
  def initialize(lines)
    @lines = parse(lines)
  end

  def as_grid
    format_str = '%3d' * (max_coordinates[1] + 1)

    vent_map.map do |row|
      "#{format_str}" % row
    end.join("\n")
  end

  def overlapping_points
    vent_map.inject(0) do |sum, row|
      sum + row.select { |col| col > 1 }.size
    end
  end

  private

  def parse(lines)
    parsed_lines = lines.map do |line|
      start_point, end_point = line.split(' -> ').map { |coords| coords.split(',').map(&:to_i) }

      {
        start: start_point,
        stop: end_point
      }
    end

    parsed_lines
  end

  def max_coordinates
    @max_coordinates ||= begin
      max_x = max_y = 0

      @lines.each do |line|
        line_max_x = [line[:start][0], line[:stop][0]].max

        max_x = line_max_x if max_x < line_max_x

        line_max_y = [line[:start][1], line[:stop][1]].max

        max_y = line_max_y if max_y < line_max_y
      end

      [max_x, max_y]
    end
  end

  def vent_map
    @vent_map ||= begin
      map_dimensions = [max_coordinates[0] + 1, max_coordinates[1] + 1]
      temp_map = map_dimensions[1].times.map { [0] * map_dimensions[0] }

      @lines.each do |line|
        points_between(line[:start], line[:stop]).each do |point|
          temp_map[point[1]][point[0]] += 1
        end
      end

      temp_map
    end
  end

  def points_between(first, second)
    # Only handle horizontal or vertical lines
    if first[0] == second[0]
      # Vertical line
      constant_x = first[0]
      y_coordinates = [first[1], second[1]].sort

      (y_coordinates[0]..y_coordinates[1]).map { |y| [constant_x, y] }
    elsif first[1] == second[1]
      # Horizontal line
      constant_y = first[1]
      x_coordinates = [first[0], second[0]].sort

      (x_coordinates[0]..x_coordinates[1]).map { |x| [x, constant_y] }
    else
      # Not considering diagonal lines yet
      []
    end
  end
end

lines = File.readlines('../../spec/fixtures/2021/input5.txt')
# lines = File.readlines('../../data/2021/input5.txt')

vent_map = VentMap.new(lines)
puts vent_map.as_grid
puts "Answer: #{vent_map.overlapping_points}"
