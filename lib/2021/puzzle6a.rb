#!/usr/bin/env ruby

def simulate(ages, days)
  latest_ages = ages.dup
  updated_ages = []

  while days.positive?
    updated_ages = []
    new_fish = []

    latest_ages.each do |age|
      if age.zero?
        updated_ages << 6
        new_fish << 8
      else
        updated_ages << age - 1
      end
    end

    latest_ages = updated_ages + new_fish
    days -= 1
  end

  latest_ages
end

# input = File.readlines('../../spec/fixtures/2021/input6.txt')
input = File.readlines('../../data/2021/input6.txt')

lanternfish_ages = input[0].split(',').map(&:to_i)

puts "Answer: Lanternfish after 80 days: #{simulate(lanternfish_ages, 80).size}"
