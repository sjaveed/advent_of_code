#!/usr/bin/env ruby

SEGMENT_COUNT = [6, 2, 5, 5, 4, 5, 6, 3, 7, 6].freeze

# ## a ##
# #     #
# b     c
# #     #
# ## d ##
# #     #
# e     f
# #     #
# ## g ##

# SEGMENT_SHARED_BETWEEN = {
#   a: [0,    2, 3,    5, 6, 7, 8, 9],
#   b: [0,          4, 5, 6,    8, 9],
#   c: [0, 1, 2, 3, 4,       7, 8, 9],
#   d: [      2, 3, 4, 5, 6,    8, 9],
#   e: [0,    2,          6,    8   ],
#   f: [0, 1,    3, 4, 5, 6, 7, 8, 9],
#   g: [0,    2, 3,    5, 6,    8, 9]
# }.freeze

# segments_1: 1_segments.first
# segments_4: 4_segments.first
# segments_7: 3_segments.first
# segments_8: 7_segments.first

# segments_2: (5_segments & segments_1).size == 1
# segments_3: (5_segments & segments_1).size == 2
# segments_5: (5_segments & segments_1).size == 0
# segments_6: (6_segments & segments_7).size == 2
# segments_9: (6_segments & segments_4).size == 4
# segments_0: (6_segments & segments_4).size == 3 && (6_segments & segments_7).size == 3

def identify_input_digits(signals)
  digits = [''] * 10

  digits[1] = signals.find { |signal| signal.size == 2 }.split('')
  digits[4] = signals.find { |signal| signal.size == 4 }.split('')
  digits[7] = signals.find { |signal| signal.size == 3 }.split('')
  digits[8] = signals.find { |signal| signal.size == 7 }.split('')

  signals_with_five_segments = signals.select { |signal| signal.size == 5 }.map { |signal| signal.split('') }

  digits[2] = signals_with_five_segments.find do |signal|
    (signal & digits[1]).size == 1 && (signal & digits[4]).size == 2
  end
  digits[3] = signals_with_five_segments.find { |signal| (signal & digits[1]).size == 2 }
  digits[5] = signals_with_five_segments.find do |signal|
    (signal & digits[1]).size == 1 &&
      (signal & digits[4]).size == 3
  end

  signals_with_six_segments = signals.select { |signal| signal.size == 6 }.map { |signal| signal.split('') }

  digits[6] = signals_with_six_segments.find { |signal| (signal & digits[7]).size == 2 }
  digits[9] = signals_with_six_segments.find { |signal| (signal & digits[4]).size == 4 }
  digits[0] = signals_with_six_segments.find do |signal|
    (signal & digits[4]).size == 3 && (signal & digits[7]).size == 3
  end

  digits
end

# SEGMENT_SHARED_BETWEEN = {
#   a: [0,    2, 3,    5, 6, 7, 8, 9],
#   b: [0,          4, 5, 6,    8, 9],
#   c: [0, 1, 2, 3, 4,       7, 8, 9],
#   d: [      2, 3, 4, 5, 6,    8, 9],
#   e: [0,    2,          6,    8   ],
#   f: [0, 1,    3, 4, 5, 6, 7, 8, 9],
#   g: [0,    2, 3,    5, 6,    8, 9]
# }.freeze

# a: segments_0 & segments_2 & segments_6 & segments_7
# WIP b: segments_4 & segments_6
# c: segments_1 & segments_2
# d: 

def map_segments(digits)
  segment_map = {}

  segment_map['a'] = [digits[0], digits[2], digits[6], digits[7]].reduce(:&)[0]
  segment_map['b'] = (digits[4] - digits[1] - digits[3])[0]
  segment_map['c'] = [digits[1], digits[2]].reduce(:&)[0]
  segment_map['d'] = [digits[2], digits[4], digits[5]].reduce(:&)[0]
  segment_map['e'] = (digits[2] - (digits[2] & digits[3]))[0]
  segment_map['f'] = (digits[3] - (digits[2] & digits[3]))[0]
  segment_map['g'] = (digits[3] - digits[4] - digits[7])[0]

  segment_map.invert
end

SEGMENTS_FOR_DIGITS = {
  abcefg:  0,
  cf:      1,
  acdeg:   2,
  acdfg:   3,
  bcdf:    4,
  abdfg:   5,
  abdefg:  6,
  acf:     7,
  abcdefg: 8,
  abcdfg:  9
}.freeze

def map_output_to_code(output, segment_map)
  output.map do |segments|
    standard_digit = segments.split('').map { |segment| segment_map[segment] }.sort.join('').to_sym

    SEGMENTS_FOR_DIGITS[standard_digit]
  end.join('').to_i
end

input = File.readlines('../../data/2021/input8.txt')
# input = File.readlines('../../spec/fixtures/2021/input8.txt')
data = input.map do |line|
  recorded_signals, output_digits = line.chomp.split(' | ')

  {
    signals: recorded_signals.split(' '),
    digits: output_digits.split(' ')
  }
end

answer = data.map do |datum|
  digits = identify_input_digits(datum[:signals])
  segment_map = map_segments(digits)

  map_output_to_code(datum[:digits], segment_map)
end.reduce(:+)

puts "Answer: #{answer}"
