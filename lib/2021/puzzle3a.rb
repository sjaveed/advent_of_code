#!/usr/bin/env ruby

codes = File.readlines('../../data/2021/input3.txt')

bit_frequencies = 12.times.map { [0, 0] } # frequency of bit 0, frequency of bit 1

codes.each do |code|
  code.chomp.split('').each_with_index do |bit, bit_idx|
    bit_frequencies[bit_idx][bit.to_i] += 1
  end
end

puts "Bit Frequencies: ", bit_frequencies

gamma_rate = bit_frequencies.map { |frequency| frequency.index(frequency.max).to_s }.join('').to_i(2)
epsilon_rate = bit_frequencies.map { |frequency| frequency.index(frequency.min).to_s }.join('').to_i(2)

puts "Gamma Rate: #{gamma_rate}"
puts "Epsilon Rate: #{epsilon_rate}"
puts "Product: #{gamma_rate * epsilon_rate}"
