#!/usr/bin/env ruby

def fuel_cost(crab_positions, position)
  crab_positions
    .map { |crab_position| (crab_position - position).abs }
    .reduce(:+)
end

def optimal_position_and_cost(crab_positions)
  min_position, max_position = crab_positions.minmax
  best_position = min_position
  best_fuel_cost = fuel_cost(crab_positions, min_position)

  ((min_position + 1)..max_position).each do |position|
    fuel_cost_for_position = fuel_cost(crab_positions, position)

    if fuel_cost_for_position < best_fuel_cost
      best_position = position
      best_fuel_cost = fuel_cost_for_position
    end
  end

  [best_position, best_fuel_cost]
end

input = File.readlines('../../data/2021/input7.txt')
# input = File.readlines('../../spec/fixtures/2021/input7.txt')
positions = input[0].split(',').map(&:to_i)

best_position, best_fuel_cost = optimal_position_and_cost(positions)

puts "Best Position: #{best_position}"
puts "Answer: #{best_fuel_cost}"
