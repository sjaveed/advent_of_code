#!/usr/bin/env ruby

# Simulator for a pod of Dumbo Octopuses.
class OctopusPod
  attr_accessor :pod, :row_size, :col_size, :flash_count

  def initialize(rows)
    self.pod = rows.map { |l| l.split('').map(&:to_i) }
    self.row_size = pod[0].size
    self.col_size = pod.size
    self.flash_count = 0
  end

  def step
    substep_idx = 0

    flashes = start_step
    # puts "  substep #{substep_idx}"
    # puts self
    # puts '----'
    substep_idx += 1

    until flashes.empty?
      flashes = intermediate_step(flashes)
      # puts "  substep #{substep_idx}"
      # puts self
      # puts '----'
      substep_idx += 1
    end

    complete_step
  end

  def to_s
    pod.map(&:join).join("\n")
  end

  private

  def start_step
    flashes_detected = []

    pod.each_with_index do |row, row_idx|
      row.each_with_index do |_octopus, octopus_idx|
        next if pod[row_idx][octopus_idx] == -1

        pod[row_idx][octopus_idx] += 1
        next unless pod[row_idx][octopus_idx] == 10

        pod[row_idx][octopus_idx] = -1
        flashes_detected << [row_idx, octopus_idx]
      end
    end

    flashes_detected
  end

  def surrounding_indexes(row_idx, col_idx)
    surrounding_idx_list = []

    (([0, row_idx - 1].max)..([row_idx + 1, self.col_size - 1].min)).each do |valid_row_idx|
      (([0, col_idx - 1].max)..([col_idx + 1, self.row_size - 1].min)).each do |valid_col_idx|
        surrounding_idx_list << [valid_row_idx, valid_col_idx] unless valid_row_idx == row_idx && valid_col_idx == col_idx
      end
    end

    surrounding_idx_list
  end

  def intermediate_step(past_flashes)
    flashes_detected = []

    past_flashes.each do |flash|
      surrounding_indexes(flash[0], flash[1]).each do |surrounding_idx|
        next if pod[surrounding_idx[0]][surrounding_idx[1]] == -1

        pod[surrounding_idx[0]][surrounding_idx[1]] += 1
        next unless pod[surrounding_idx[0]][surrounding_idx[1]] == 10

        pod[surrounding_idx[0]][surrounding_idx[1]] = -1
        flashes_detected << surrounding_idx
      end
    end

    flashes_detected
  end

  def complete_step
    self.flash_count = 0

    pod.each_with_index do |row, row_idx|
      row.each_with_index do |_octopus, octopus_idx|
        if pod[row_idx][octopus_idx] == -1
          self.flash_count += 1
          pod[row_idx][octopus_idx] = 0
        end
      end
    end
  end
end

input = File.readlines('../../data/2021/input11.txt')
# input = File.readlines('../../spec/fixtures/2021/input11.txt')
pod = OctopusPod.new(input.map(&:chomp))

flashes = 0

100.times do |i|
  # puts "Step #{i}"
  pod.step
  flashes += pod.flash_count
  # puts pod
  # puts "Flashes: #{pod.flash_count}"
  # puts "===="
end

puts "Answer: #{flashes}"
