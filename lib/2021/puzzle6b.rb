#!/usr/bin/env ruby

def simulate(ages, days)
  age_map = ages.group_by { |age| age }.transform_values(&:size)

  day_idx = 0

  while days.positive?
    updated_age_map = (0..8).map { |age| [age, 0] }.to_h
    new_fish = 0

    age_map.each do |age, frequency|
      if age.zero?
        updated_age_map[6] = frequency
        new_fish += frequency
      else
        updated_age_map[age - 1] += frequency
      end
    end

    updated_age_map[8] = new_fish
    age_map = updated_age_map.dup
    puts "Day #{day_idx}: #{updated_age_map.values.reduce(:+)}"
    day_idx += 1
    days -= 1
  end

  age_map
end

# input = File.readlines('../../spec/fixtures/2021/input6.txt')
input = File.readlines('../../data/2021/input6.txt')

lanternfish_ages = input[0].split(',').map(&:to_i)

puts "Answer: Lanternfish after 256 days: #{simulate(lanternfish_ages, 256).values.reduce(:+)}"
