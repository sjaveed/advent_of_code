#!/usr/bin/env ruby

instructions = File.readlines("../../data/2021/input2.txt")

def move(direction, amount, &block)
  x = 0
  y = 0
  z = 0

  case direction
  when :forward
    x += amount
  when :up
    z -= amount
  when :down
    z += amount
  end

  yield x, y, z
end

position = {
  x: 0,
  y: 0,
  z: 0
}

instructions.each do |instruction|
  direction, amount = instruction.split ' '

  move(direction.to_sym, amount.to_i) do |delta_x, delta_y, delta_z|
    position[:x] += delta_x
    position[:y] += delta_y
    position[:z] += delta_z
  end
end

puts "Position: ", position
puts "Product: #{position[:x] * position[:z]}"
