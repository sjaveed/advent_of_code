#!/usr/bin/env ruby

depths = File.readlines("../../data/2021/input1.txt").map { |depth| depth.to_i }

window_size = 3
last_group = window_size.times.map { depths.shift }
last_total_depth = last_group.reduce(:+)
depth_increases = 0

while depths.any?
  depth = depths.shift
  total_depth = last_total_depth - last_group.shift + depth

  depth_increases += 1 if total_depth > last_total_depth
  last_group << depth
end

puts "Depth Increases: #{depth_increases}"
