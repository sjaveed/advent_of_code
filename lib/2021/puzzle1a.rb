#!/usr/bin/env ruby

depths = File.readlines("../../data/2021/input1.txt").map { |depth| depth.to_i }

last_depth = depths.shift
depth_increases = 0

while depths.any?
  depth = depths.shift
  depth_increases += 1 if depth > last_depth
  last_depth = depth
end

puts "Depth Increases: #{depth_increases}"
