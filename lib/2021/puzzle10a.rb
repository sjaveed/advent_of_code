#!/usr/bin/env ruby

# Calculate the score for a line.
class SyntaxScorer
  attr_accessor :line

  ERROR_VALUE = {
    ')' => 3,
    ']' => 57,
    '}' => 1197,
    '>' => 25_137
  }.freeze

  OPENING_CHAR = %/([{</.freeze
  CLOSING_CHAR = %/)]}>/.freeze
  MATCHING_OPENING_CHAR = {
    ')' => '(',
    ']' => '[',
    '}' => '{',
    '>' => '<'
  }.freeze

  def initialize(line)
    self.line = line
  end

  def score
    return 0 unless error

    ERROR_VALUE[error]
  end

  private

  def error
    stack = []
    chars = line.split ''

    until chars.empty?
      char = chars.shift

      if OPENING_CHAR.include? char
        stack.push char
      elsif CLOSING_CHAR.include? char
        opening_char = stack.pop

        return char if opening_char != MATCHING_OPENING_CHAR[char]
      end
    end
  end
end

input = File.readlines('../../data/2021/input10.txt')
# input = File.readlines('../../spec/fixtures/2021/input10.txt')
scores = input.map { |line| SyntaxScorer.new(line).score }

puts "Answer: #{scores.sum}"
