#!/usr/bin/env ruby

class HeightMap
  attr_accessor :low_points

  def initialize(heights)
    @heights = heights
    @low_points = []
  end

  def calculate_low_points
    @heights.each_with_index do |row, row_idx|
      row.each_with_index do |col, col_idx|
        if col < neighbors(col_idx, row_idx).min
          @low_points << [row_idx, col_idx]
        end
      end
    end
  end

  def total_risk_level
    low_point_values.map { |low_point_value| low_point_value + 1 }.reduce(:+)
  end

  private

  def low_point_values
    @low_points.map { |row, col| @heights[row][col] }
  end

  def neighbors(col, row)
    calculated_neighbors = []

    calculated_neighbors << case col
    when 0
      @heights[row][col + 1]
    when max_col
      @heights[row][col - 1]
    else
      [@heights[row][col - 1], @heights[row][col + 1]]
    end

    calculated_neighbors << case row
    when 0
      @heights[row + 1][col]
    when max_row
      @heights[row - 1][col]
    else
      [@heights[row - 1][col], @heights[row + 1][col]]
    end

    calculated_neighbors.flatten
  end

  def max_row
    @max_row ||= @heights.size - 1
  end

  def max_col
    @max_col ||= @heights[0].size - 1
  end
end

input = File.readlines('../../data/2021/input9.txt')
# input = File.readlines('../../spec/fixtures/2021/input9.txt')
heights = input.map { |line| line.chomp.split('').map(&:to_i) }
height_map = HeightMap.new(heights)
height_map.calculate_low_points

puts "Answer: #{height_map.total_risk_level}"
