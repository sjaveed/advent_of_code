#!/usr/bin/env ruby

SEGMENT_COUNT = [6, 2, 5, 5, 4, 5, 6, 3, 7, 6]

def digit_count(collected_data, digits = [])
  segment_counts = digits.map { |digit| SEGMENT_COUNT[digit] }

  collected_data
    .flat_map { |data| data[:digits].map(&:size).select { |segment_count| segment_counts.include?(segment_count) } }
    .size
end

input = File.readlines('../../data/2021/input8.txt')
# input = File.readlines('../../spec/fixtures/2021/input8.txt')
data = input.map do |line|
  recorded_signals, output_digits = line.chomp.split(' | ')

  {
    signals: recorded_signals.split(' '),
    digits: output_digits.split(' ')
  }
end

puts "Answer: #{digit_count(data, [1, 4, 7, 8])}"
