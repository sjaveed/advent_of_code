#!/usr/bin/env ruby

# Calculate the score for a line.
class SyntaxScorer
  attr_accessor :line, :stack

  ERROR_VALUE = {
    ')' => 3,
    ']' => 57,
    '}' => 1197,
    '>' => 25_137
  }.freeze

  COMPLETION_VALUE = {
    ')' => 1,
    ']' => 2,
    '}' => 3,
    '>' => 4
  }.freeze

  OPENING_CHAR = %/([{</.freeze
  CLOSING_CHAR = %/)]}>/.freeze
  MATCHING_OPENING_CHAR = {
    ')' => '(',
    ']' => '[',
    '}' => '{',
    '>' => '<'
  }.freeze

  MATCHING_CLOSING_CHAR = MATCHING_OPENING_CHAR.map { |k, v| [v, k] }.to_h.freeze

  def initialize(line)
    self.line = line
  end

  def valid?
    return false if error

    true
  end

  def completion_score
    score = 0

    self.stack.reverse.each do |char|
      char_score = COMPLETION_VALUE[MATCHING_CLOSING_CHAR[char]]

      score = score * 5 + char_score
    end

    score
  end

  private

  def error
    self.stack = []
    chars = line.split ''

    until chars.empty?
      char = chars.shift

      if OPENING_CHAR.include? char
        self.stack.push char
      elsif CLOSING_CHAR.include? char
        opening_char = self.stack.pop

        return char if opening_char != MATCHING_OPENING_CHAR[char]
      end
    end
  end
end

input = File.readlines('../../data/2021/input10.txt')
# input = File.readlines('../../spec/fixtures/2021/input10.txt')
scores = input.map { |line| SyntaxScorer.new(line) }.select(&:valid?).map(&:completion_score).sort

puts "Answer: #{scores[scores.size / 2]}"
