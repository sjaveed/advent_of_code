#!/usr/bin/env ruby

instructions = File.readlines("../../data/2021/input2.txt")

def move(aim, direction, amount, &block)
  x = 0
  y = 0
  z = 0

  case direction
  when :forward
    x += amount
    z += (aim * amount)
  when :up
    aim -= amount
  when :down
    aim += amount
  end

  yield x, y, z, aim
end

position = {
  x: 0,
  y: 0,
  z: 0,
  aim: 0
}

instructions.each do |instruction|
  direction, amount = instruction.split ' '

  move(position[:aim], direction.to_sym, amount.to_i) do |delta_x, delta_y, delta_z, new_aim|
    position[:x] += delta_x
    position[:y] += delta_y
    position[:z] += delta_z
    position[:aim] = new_aim
  end
end

puts "Position: ", position
puts "Product: #{position[:x] * position[:z]}"
