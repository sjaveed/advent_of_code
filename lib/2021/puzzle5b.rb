#!/usr/bin/env ruby

class VentMap
  def initialize(lines)
    @lines = parse(lines)
  end

  def as_grid
    format_str = '%3d' * (max_coordinates[1] + 1)

    vent_map.map do |row|
      "#{format_str}" % row
    end.join("\n")
  end

  def overlapping_points
    vent_map.inject(0) do |sum, row|
      sum + row.select { |col| col > 1 }.size
    end
  end

  private

  def parse(lines)
    lines.map do |line|
      start_point, end_point = line.split(' -> ').map { |coords| coords.split(',').map(&:to_i) }

      {
        start: start_point,
        stop: end_point
      }
    end
  end

  def max_coordinates
    @max_coordinates ||= begin
      max_x = max_y = 0

      @lines.each do |line|
        line_max_x = [line[:start][0], line[:stop][0]].max

        max_x = line_max_x if max_x < line_max_x

        line_max_y = [line[:start][1], line[:stop][1]].max

        max_y = line_max_y if max_y < line_max_y
      end

      [max_x, max_y]
    end
  end

  def vent_map
    @vent_map ||= begin
      map_dimensions = [max_coordinates[0] + 1, max_coordinates[1] + 1]
      temp_map = map_dimensions[1].times.map { [0] * map_dimensions[0] }

      @lines.each do |line|
        points_between(line[:start], line[:stop]).each do |point|
          temp_map[point[1]][point[0]] += 1
        end
      end

      temp_map
    end
  end

  def points_between(first, second)
    delta_x = second[0] - first[0]
    delta_y = second[1] - first[1]
    steps = [delta_x.abs, delta_y.abs].max
    step_x = delta_x / steps
    step_y = delta_y / steps

    points = []
    intermediate_point = first.dup

    until intermediate_point[0] == second[0] && intermediate_point[1] == second[1]
      points << intermediate_point.dup

      intermediate_point[0] += step_x
      intermediate_point[1] += step_y
    end

    points << intermediate_point.dup

    points
  end
end

# lines = File.readlines('../../spec/fixtures/2021/input5.txt')
lines = File.readlines('../../data/2021/input5.txt')

vent_map = VentMap.new(lines)
# puts vent_map.as_grid
puts "Answer: #{vent_map.overlapping_points}"
